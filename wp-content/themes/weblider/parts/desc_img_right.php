<?php
$desc = get_sub_field('desc');
$img = get_sub_field('img');
?>

<section class="page_loop desc_img_right">
  <div class="page_loop_single">
    <div class="content">
      <?php echo $desc ?>
    </div>
    <div class="img" style="background-image: url(<?php echo $img['url'] ?>)">

    </div>
  </div>
</section>