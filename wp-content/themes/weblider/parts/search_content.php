<div class="products-wrapper mt-3 pt-3">
    <div class="images-container">
        <?php require 'paginate/images.php'; ?>
    </div>
    <div class="pagi-images" data-fill=".images-container" data-url="/wp-json/wp/v1/images" data-method="POST">
        <div class="col-12 pagination_container">
            <ul class="pagination">
                <li class="prev-page disable">
                    <span class="nospan">Previous</span>
                </li>
                <div class="number-content">
                    <li class="pagi-curr number-page">
                        <a class="pagi active-pagi" href="#">1</a>
                    </li>
                    <?php if ($AllImages->post_count / 8 > 1) { ?>
                        <?php for ($i = 1; $i <= $AllImages->post_count / 8; $i++) { ?>
                            <li class="number-page">
                                <a class="pagi" href="#"><?php echo $i + 1; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </div>
                <li class="next-page">
                    <a href="#">Next</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="products-wrapper mt-3 pt-3">
    <div class="videos-container">
        <?php require 'paginate/videos.php'; ?>
    </div>
    <div class="pagi-videos" data-fill=".videos-container" data-url="/wp-json/wp/v1/videos" data-method="POST">
        <div class="col-12 pagination_container">
            <ul class="pagination">
                <li class="prev-page disable">
                    <span class="nospan">Previous</span>
                </li>
                <div class="number-content">
                    <li class="pagi-curr number-page">
                        <a class="pagi active-pagi" href="#">1</a>
                    </li>
                    <?php if ($AllVideos->post_count / 8 > 1) { ?>
                        <?php for ($i = 1; $i <= $AllVideos->post_count / 8; $i++) { ?>
                            <li class="number-page">
                                <a class="pagi" href="#"><?php echo $i + 1; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </div>
                <li class="next-page">
                    <a href="#">Next</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="blog-wrapper mt-3 pt-3">
    <div class="files-container">
        <?php require 'paginate/files.php'; ?>
    </div>
    <div class="pagi-files" data-fill=".files-container" data-url="/wp-json/wp/v1/files" data-method="POST">
        <div class="col-12 pagination_container">
            <ul class="pagination">
                <li class="prev-page disable">
                    <span class="nospan">Previous</span>
                </li>
                <div class="number-content">
                    <li class="pagi-curr number-page">
                        <a class="pagi active-pagi" href="#">1</a>
                    </li>
                    <?php if ($AllFiles->post_count / 8 > 1) { ?>
                        <?php for ($i = 1; $i <= $AllFiles->post_count / 8; $i++) { ?>
                            <li class="number-page">
                                <a class="pagi" href="#"><?php echo $i + 1; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </div>
                <li class="next-page">
                    <a href="#">Next</a>
                </li>
            </ul>
        </div>
    </div>
</div>