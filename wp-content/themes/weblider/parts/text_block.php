<?php
$text = get_sub_field('text');
?>
<section class="text_block">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php echo $text ?>
      </div>
    </div>
  </div>
</section>