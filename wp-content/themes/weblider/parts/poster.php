<div class="gallery">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php
        $images = get_sub_field('poster_images');
        if( $images ): ?>
            <style>.poster{padding:0;}</style>
            <div class="container"><div class="row">
                <?php foreach( $images as $image ): ?>
                    <div class="col-12 col-md-6 col-lg-4 poster">
                      <img src="<?php echo ($image['poster_image']['url'])?>">
                    </div>
                <?php endforeach; ?>
            </div></div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
