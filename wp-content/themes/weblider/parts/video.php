<?php

$video = get_sub_field('video');

$desc_video = get_sub_field('desc_video');

preg_match('/src="(.+?)"/', $video, $matches_url);

$src = $matches_url[1];

preg_match('/embed(.*?)?feature/', $src, $matches_id);

$id = $matches_id[1];

$id = str_replace(str_split('?/'), '', $id);

preg_match('/src="(.+?)"/', $video, $matches);

$src = $matches[1];

$params = array(

  'controls'    => 1,

  'hd'        => 1,

  'autohide'    => 1

);

$new_src = add_query_arg($params, $src);

$iframe = str_replace($src, $new_src, $video);

$attributes = 'frameborder="0"';

$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

?>

<section class="video_container">

  <div class="container">

    <div class="row">

      <div class="col-12">


        <div class="single-film" style="background-image: url('https://img.youtube.com/vi/<?php echo $id; ?>/hqdefault.jpg');">

          <div class="embed-container" data-embed="<?php echo $id; ?>">

            <div class="desc-film">

              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/play@2.png" />

              <?php echo $desc_video ?>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>