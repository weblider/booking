<div class="gallery">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php
        $images = get_sub_field('gallery');
        if( $images ): ?>
            <ul>
                <?php foreach( $images as $image ): ?>
                    <li>
                        <a href="<?php echo esc_url($image['url']); ?>" data-rel="lightcase:myCollection">
                             <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
