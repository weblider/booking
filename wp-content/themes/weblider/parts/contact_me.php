<section>
    <div class="container">
        <div class="row">
            <div class="my-0 mx-auto text-center">
                <br>
                <div class="btn btn-green"><a class="btn-green" id="book">Book now</a></div>
                <br>
            </div>
        </div>
        <div class="row" id="calendar">
            <div class="my-0 mx-auto">
                <br>
                <?php the_content(); ?>
                <br>
            </div>
        </div>
    </div>
</section>
