<?php
$title = get_sub_field('title');
function getYoutubeVideoId($iframeCode)
{

  // Extract video url from embed code
  return preg_replace_callback('/<iframe\s+.*?\s+src=(".*?").*?<\/iframe>/', function ($matches) {
    // Remove quotes
    $youtubeUrl = $matches[1];
    $youtubeUrl = trim($youtubeUrl, '"');
    $youtubeUrl = trim($youtubeUrl, "'");
    // Extract id
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $youtubeUrl, $videoId);
    return $youtubeVideoId = isset($videoId[1]) ? $videoId[1] : "";
  }, $iframeCode);
}
?>
<style>
  @media (max-width:500px) {
    .swiper-slide {
      width: 100% !important;
    }
  }
</style>
<section class="video_photo">
  <div class="container">
    <div class="row">
      <div class="col-12 title">
        <?php echo $title ?>
      </div>
      <div class="col-12 slider">
        <?php if (have_rows('video_photo')) : ?>
          <div class="swiper-container swiper_video_photo">
            <div class="swiper-wrapper">
              <?php while (have_rows('video_photo')) : the_row();
                $img = get_sub_field('img');
                // Load value.
                $iframe = get_sub_field('video');
                $iframee = get_sub_field('video');
                // Use preg_match to find iframe src.
                preg_match('/src="(.+?)"/', $iframe, $matches);
                $src = $matches[1] ?? null;
                // Add extra parameters to src and replcae HTML.
                $params = array(
                  'controls'  => 0,
                  'hd'        => 1,
                  'autohide'  => 0
                );
                $new_src = add_query_arg($params, $src);
                $iframe = str_replace($src, $new_src, $iframe);
                // Add extra attributes to iframe HTML.
                $attributes = 'frameborder="0" data-rel="lightcase:myCollection';
                $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
                // Display customized HTML.

              ?>
                <div class="swiper-slide">
                  <div class="swiper_content">
                    <?php
                    if (!empty($img) && empty($src)) { ?>
                      <a href="<?php echo $img['url'] ?>" data-rel="lightcase:myCollection">
                        <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
                      </a>
                    <?php } else { ?>
                      <div class="embed-container">
                        <a class="example-image-link" target="_blank" href="<?php echo $src; ?>" data-rel="lightcase:myCollection" data-video="true" data-title="Click anywhere outside the image or the X to the right to close.">
                          <?php if (!get_sub_field('img')) { ?>
                            <img class="example-image" src="https://img.youtube.com/vi/<?php echo getYoutubeVideoId($iframe); ?>/mqdefault.jpg" alt="" />
                          <?php } else { ?>
                            <img class="example-image" src="<?php echo get_sub_field('img')['url']; ?>" alt="" />
                          <?php } ?>
                        </a>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>