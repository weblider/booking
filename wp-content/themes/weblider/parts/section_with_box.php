<?php
$title_section = get_sub_field('title_section');
?>
<section class="section_with_box">
  <div class="container">
    <div class="row">
      <div class="col-12 title">
        <?php echo $title_section ?>
      </div>
      <?php if (have_rows('box')) : ?>
        <?php while (have_rows('box')) : the_row();
          $desc = get_sub_field('desc');
        ?>
          <div class="col-md-4 single_box">
            <?php echo $desc ?>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</section>