<?php
$img = get_sub_field('img');
$desc = get_sub_field('desc');
?>

<div class="big_img">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
        <?php
        if (!empty($desc)) { ?>
          <div class="label">
            <p><?php echo $desc ?></p>
          </div>
        <?php }
        ?>
      </div>
    </div>
  </div>
</div>