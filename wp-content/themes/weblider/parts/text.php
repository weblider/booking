<?php
$desc = get_sub_field('desc');
?>
<div class="desc_block">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php echo $desc ?>
      </div>
    </div>
  </div>
</div>