<?php
$query3 = array(
    'post_type'      => 'attachment',
    'cat' => 3,
    'post_status'    => 'inherit',
    'posts_per_page' => -1,
);
$AllFiles = new WP_Query($query3);

if(isset($data)){
    if(isset($data['files'])){
        $result = $data['files']->posts;
        $count = $data['files']->post_count;;;
    }
} else{
    $query3 = array(
        'post_type'      => 'attachment',
        'cat' => 3,
        'post_status'    => 'inherit',
        'posts_per_page' => 4,
    );
    $files = new WP_Query($query3);
    $result = $files->posts;
    $count = $AllFiles->post_count;;
}

?>
<h2 class="blog-title">Files (found <?php echo $count; ?>)</h2>
<div class="row-alt">
    <?php if ($result) { ?>
        <?php foreach ($result as $file) {  ?>
            <a href="<?php echo $file->guid; ?>">
                <div class="d-flex flex-row">
                    <div class="img">
                        <img style="width:175px; height: auto" src="/wp-content/themes/weblider/assets/img/pdf.png" alt="">
                    </div>
                    <div class="text-video">
                        <h3><?php echo $file->post_title; ?></h3>
                        <p><?php echo $file->post_excerpt; ?></p>
                    </div>
                </div>
            </a>
        <?php } ?>
    <?php } else { ?>
        <p class="text-center">Nothing here</p>
    <?php } ?>
</div>