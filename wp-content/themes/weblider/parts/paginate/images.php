<?php
$query = array(
    'post_type'      => 'attachment',
    'post_mime_type' => 'image',
    'cat' => 2,
    'post_status'    => 'inherit',
    'posts_per_page' => -1,
);
$AllImages = new WP_Query($query);

if(isset($data)){
    if(isset($data['images'])){
        $result = $data['images']->posts;
        $count = $data['images']->post_count;;
    }
} else{
    $query = array(
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'cat' => 2,
        'post_status'    => 'inherit',
        'posts_per_page' => 8,
    );
    $images = new WP_Query($query);
    $result = $images->posts;
    $count = $AllImages->post_count;
}

?>
<h2 class="products-title">Images (found <?php echo $count; ?>)</h2>
<div class="row">
    <?php if ($result) { ?>
        <?php foreach ($result as $img) { ?>
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="card-img-top">
                    <img src="<?php echo $img->guid; ?>" alt="">
                </div>
                <h3 class="card-text"><?php echo $img->post_title; ?></h3>
            </div>
        <?php } ?>
    <?php } else { ?>
        <p class="text-center">Nothing here</p>
    <?php } ?>
</div>