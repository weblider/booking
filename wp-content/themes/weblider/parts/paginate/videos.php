<?php

$query2 = array(
    'post_type'      => 'attachment',
    'cat' => 4,
    'post_status'    => 'inherit',
    'posts_per_page' => -1,
);
$AllVideos = new WP_Query($query2);


if(isset($data)){
    if(isset($data['videos'])){
        $result = $data['videos']->posts;
        $count = $data['videos']->post_count;
    }
} else{
    $query2 = array(
        'post_type'      => 'attachment',
        'cat' => 4,
        'post_status'    => 'inherit',
        'posts_per_page' => 8,
    );
    $videos = new WP_Query($query2);
    $result = $videos->posts;
    $count = $AllVideos->post_count;
}

?>

<h2 class="products-title">Video (found <?php echo $count; ?>)</h2>
<div class="row">
    <?php if ($result) { ?>
        <?php foreach ($result as $img) { ?>
            <div class="col-lg-3 col-md-4 col-sm-12">
                <div class="card-img-top">
                    <video src="<?php echo $img->guid; ?>"></video>
                </div>
                <h3 class="card-text" style="word-break: break-all"><?php echo $img->post_title; ?></h3>
            </div>
        <?php } ?>
    <?php } else { ?>
        <p class="text-center">Nothing here</p>
    <?php } ?>
</div>