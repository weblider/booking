<div class="gallery_with_big">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php
        $images = get_sub_field('gallery_with_big');
        if( $images ): $count = 0;?>
            <ul>
                <?php foreach( $images as $image ):
                  $count++;
                if( $count == 5 || $count == 10 ) {
                  $class = 'big';
                } else {
                  $class = '';
                }
                ?>
                    <li class="<?php echo $class ?>">

                        <a href="<?php echo esc_url($image['url']); ?>" data-rel="lightcase:myCollection">
                             <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        </a>
                    </li>
                    <?php
                    if( $count == 12 ) {
                      $count = 0;
                    } else {
                      $count == $count;
                    }
                    ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
