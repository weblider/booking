<?php
/* Template name: Front-page */
?>
<?php get_header();

$args = array(
    'post_type' => 'post',
    'category_name' => 'machines',
    'posts_per_page' => -1,
);
$wp_query = new WP_Query($args);
?>
<section class="thumbnail" style="background-image: url('https://grafotronic.se/wp-content/uploads/2020/11/products-header.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-12 title">
                <h1><?php echo get_field('title'); ?></h1>
            </div>
        </div>
    </div>
</section>
<div class="news_page">
    <div class="container">
        <div class="row">
            <div class="col-12 ">
                <?php echo get_field('main_title'); ?>
                <p class="pb-5 pt-3"><?php echo get_field('main_subtitle'); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 filter_container">
                <div class="filter_news">
                    <div class="category_filter">
                        <div class="single_cat ">
                            <h3><?php echo get_field('showcase_description'); ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="list_post">
    <div class="container">
        <div class="row">
            <?php
            if ($wp_query->have_posts()) {
                while ($wp_query->have_posts()) {
                    $wp_query->the_post();
            ?>
                    <div class="col-md-6 col-lg-4 col-sm-12 col-xl-4 single_post">
                        <a href="<?php the_permalink(); ?>">
                            <div class="background">
                                <div class="zoom">
                                    <div class="thumbnail_post d-flex">
                                        <img class="align-self-center" src="<?php the_post_thumbnail(); ?>
                                    </div>
                                </div>
                                <div class=" content">
                                        <h3><?php the_title(); ?></h3>
                                        <p><?php echo the_excerpt(); ?></p>
                                        <div class="date">
                                            <p>Find more</p>
                                        </div>
                                    </div>
                                </div>
                        </a>
                    </div>
            <?php }
            }  ?>
        </div>
    </div>
</div>
<?php get_footer();
