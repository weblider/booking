<?php
/* Template name: Pojedynczy produkt*/
?>
<?php get_header(); ?>

<?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
$title_product = get_field('title_product');
$img_product = get_field('img_product');
$bg_page = get_field('bg_page');
?>
<style>
    header::before {
        background-color: #222;
    }

    #contact-me {
        font-weight: 600;
        border-radius: 15px;
    }
</style>
<section class="thumbnail thumbnail_products" style="background-image: url('<?php echo $bg_page['url']; ?>')">
    <div class="container">
        <div class="row">
            <div class="col-12 breadcrumb_container">
            </div>
            <div class="col-12">
                <?php
                if (!empty($title_product)) {
                    echo $title_product;
                } else { ?>
                    <h1><?php echo the_title(); ?></h1>
                <?php }
                ?>
            </div>
            <?php
            if (!empty($img_product)) { ?>
                <div class="col-12 img_product">
                    <img data-wow-delay="0.3s" src="<?php echo $img_product['url'] ?>" alt="<?php echo $img_product['alt'] ?>">
                </div>
            <?php }
            ?>
        </div>
    </div>
</section>
<section class="page_header">
    <?php if (have_rows('repeater_layout')) : $countHeader = 0; ?>
        <?php while (have_rows('repeater_layout')) : the_row();
            $title_section = get_sub_field('title_section');
            // var_dump($title_section);

            $title_formating = str_replace('&', '_', strtolower($title_section));
            $title_formating2 = str_replace(' ', '_', strtolower($title_formating));
            $title_formating3 = str_replace(',', '_', strtolower($title_formating2));

            $countHeader++;

        ?>
            <div class="single_link">
                <a href="<?php echo $title_formating3 ?>"><?php echo $title_section ?></a>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</section>
<div class="page_flexible">
    <?php if (have_rows('repeater_layout')) : ?>
        <?php while (have_rows('repeater_layout')) : the_row();
            $title_section = get_sub_field('title_section');
            $title_formating = str_replace('&', '_', strtolower($title_section));
            $title_formating2 = str_replace(' ', '_', strtolower($title_formating));
            $title_formating3 = str_replace(',', '_', strtolower($title_formating2));
        ?>
            <div class="flex_section <?php echo $title_formating3 ?>" name="<?php echo $title_formating3 ?>" id="<?php echo $title_formating3 ?>">
                <?php
                $prevSection = false;
                if (have_rows('layout')) :
                    while (have_rows('layout')) : the_row();
                        if (get_row_layout() == 'video') :
                            get_template_part('/parts/video');
                        endif;
                        if (get_row_layout() == 'text_block') : {
                                get_template_part('/parts/text_block');
                                get_template_part('/parts/contact_me');
                            }
                        endif;
                        if (get_row_layout() == 'section_with_box') : {
                                get_template_part('/parts/section_with_box');
                                if ($prevSection == true) echo '<style>.section_with_box:nth-of-type(2){padding-top: 0; padding-bottom: 100px;}</style>';
                                $prevSection = true;
                            }
                        else : $prevSection = false;
                        endif;
                        if (get_row_layout() == 'desc_img_right') :
                            get_template_part('/parts/desc_img_right');
                        endif;
                        if (get_row_layout() == 'desc_img_left') :
                            get_template_part('/parts/desc_img_left');
                        endif;
                        if (get_row_layout() == 'video_photos') :
                            get_template_part('/parts/video_photos');
                        endif;
                    // End loop.
                    endwhile;
                // No value.
                else :
                // Do something...
                endif; ?>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<style>
    #amelia-app-booking0 {
        position: absolute;
        z-index: -1;
        top: -100%;
        height: 1% !important;
    }
</style>
<?php get_footer();
