<?php get_header(); ?>
<section class="thumbnail" style="background-image: url('https://grafotronic.se/wp-content/uploads/2020/11/products-header.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-12 title">
                <h1><?php echo get_field('title'); ?></h1>
            </div>
        </div>
    </div>
</section>
<div class="container pt-5">
    <?php the_content(); ?>
</div>
<?php get_footer(); ?>