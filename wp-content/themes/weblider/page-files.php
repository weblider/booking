<?php
/*
Template Name: Wyszukiwarka
*/
get_header();

?>
<section class="thumbnail" style="background-image: url('https://grafotronic.se/wp-content/uploads/2020/11/products-header.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-12 title">
                <h1>GRAFOTRONIC</h1>
            </div>
        </div>
    </div>
</section>
<div class="search-container">
    <div class="container p-5">
        <div class="search-title-wrapper">
            <h2 class="search-title text-center">Search Page</h2>
        </div>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" id="search_input" name="name" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
        <div class="main-wrapper">
            <?php require 'parts/search_content.php' ?>
        </div>
    </div>
</div>


<style>
    a {
        text-decoration: none !important;
    }

    .active-pagi {
        padding: 13px 60px !important;
        background-color: #72c057 !important;
        color: #fff !important;
    }
</style>
<?php get_footer(); ?>