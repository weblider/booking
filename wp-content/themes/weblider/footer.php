<footer>
    <div class="box_desc">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg  box_desc_single">
                    <p>GRAFOTRONIC AB<br>
                        Box 138<br>
                        SE-261 22 Landskrona | Sweden</p>
                    <p>Phone: +46 (0) 418 567 00<br>
                        E-mail: <a href="mailto:sales@grafotronic.se">sales@grafotronic.se</a></p>
                </div>
                <div class="col-lg  box_desc_single">
                    <p>GRAFOTRONIC Sp. z o.o.<br>
                        Annopol 4a, building E<br>
                        03-236 Warsaw | Poland</p>
                    <p>Phone: +48 22 45 67 400<br>
                        E-mail: <a href="mailto:office@grafotronic.se">office@grafotronic.se </a></p>
                </div>
                <div class="col-lg  box_desc_single">
                    <p>GRAFOTRONIC Inc.<br>
                        480 East Route 22<br>
                        Lake Zurich, IL 60047 | USA</p>
                    <p>Phone: 1-847-702-9001<br>
                        E-mail: <a href="mailto:mb@grafotronic.se">mb@grafotronic.se</a></p>
                </div>
                <div class="col-lg box_desc_single">
                    <p>GRAFOTRONIC Italia Srl<br>
                        Viale Caduti del Lavoro 36<br>
                        25030 Coccaglio (BS)</p>
                    <p>Phone: +39 030 723743<br>
                        E-mail: <a href="mailto:gg@grafotronic.se">gg@grafotronic.se</a></p>
                </div>
                <div class="col-lg  box_desc_single">
                    <p class="v1MsoNormal"><span lang="PL">GRAFOTRONIC SEA<br>
                        </span>909 Ample Tower, Room no. 4/6<br>
                        Bangna-Trad KM.4, Bangna<br>
                        Bangkok 10260 | Thailand</p>
                    <p class="v1MsoNormal">Phone: +66 982650847<br>
                        E-mail: <span lang="PL"><a href="mailto:ri@grafotronic.se" rel="noreferrer"><span lang="EN-US">ri@grafotronic.se</span></a></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_down">
        <div class="container">
            <div class="row">
                <div class="col-md-4 footer_down_text">
                    <p>Learn about our<br>
                        <a href="https://grafotronic.se/cookies/" target="_blank" rel="noopener">Privacy Policy and cookies on this site</a>
                    </p>
                </div>
                <div class="col-md-4 social_box">
                    <div class="social_box_single">
                        <a href="https://www.linkedin.com/company/grafotronic">
                            <img src="https://grafotronic.se/wp-content/uploads/2020/11/social-linkedin@2.png" alt="">
                        </a>
                    </div>
                    <div class="social_box_single">
                        <a href="https://www.facebook.com/Grafotronic">
                            <img src="https://grafotronic.se/wp-content/uploads/2020/11/social-facebook@2.png" alt="">
                        </a>
                    </div>
                    <div class="social_box_single">
                        <a href="https://www.youtube.com/user/GrafotronicMachines/videos">
                            <img src="https://grafotronic.se/wp-content/uploads/2020/11/social-youtube@2.png" alt="">
                        </a>
                    </div>
                    <div class="social_box_single">
                        <a href="https://twitter.com/Grafotronic">
                            <img src="https://grafotronic.se/wp-content/uploads/2020/11/social-twitter@2.png" alt="">
                        </a>
                    </div>
                    <div class="social_box_single">
                        <a href="https://www.instagram.com/grafotronic/">
                            <img src="https://grafotronic.se/wp-content/uploads/2020/11/social-instagram@2.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-md-4 copryright">
                    <p>Copyright: Grafotronic</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="drawer-overlay drawer-toggle">

</div>
<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/dist/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/dist/js/swiper.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/dist/js/lightcase.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/dist/js/paginate.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/dist/js/script.js"></script>




<script>
    const btnGreen = document.getElementById('book');
    const calendar = document.getElementById('calendar');
    let prawda = true;

    if (btnGreen) {
        btnGreen.addEventListener('click', () => {
            if (prawda) {
                $("#amelia-app-booking0").css("position", "static");
                $("#amelia-app-booking0").css("height", "100% !important");
                prawda = false;
            } else {
                $("#amelia-app-booking0").css("position", "absolute");
                $("#amelia-app-booking0").css("height", "1% !important");
            }
        })
    }
</script>
</body>

</html>