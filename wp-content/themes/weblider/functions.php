<?php

add_theme_support('post-thumbnails');

function render($template, Array $data) {
    extract($data);

    ob_start();
        include( $template);
        $content = ob_get_contents();
    ob_end_clean();

    return $content;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v1', '/search/(?P<slug>[^.]+)', array(
        'methods' => 'POST',
        'callback' => 'search'
    ) );
} );
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v1', '/search', array(
        'methods' => 'POST',
        'callback' => 'search'
    ) );
} );


//paginacja
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v1', '/images/(?P<slug>[^.]+)', array(
        'methods' => 'POST',
        'callback' => 'paginateImages'
    ) );
} );
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v1', '/videos/(?P<slug>[^.]+)', array(
        'methods' => 'POST',
        'callback' => 'paginateVideos'
    ) );
} );
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v1', '/files/(?P<slug>[^.]+)', array(
        'methods' => 'POST',
        'callback' => 'paginateFiles'
    ) );
} );

function wpb_custom_new_menu() {
    register_nav_menus(
        array(
            'main-menu' => __( 'Main Menu' ),
        )
    );
}

add_action( 'init', 'wpb_custom_new_menu' );

 function search( $data ) {
     $query = array(
         'post_type'      => 'attachment',
         'post_mime_type' => 'image',
         'cat' => 2,
         's' => $data['slug'],
         'post_status'    => 'inherit',
         'posts_per_page' => 8,
     );
     $images = new WP_Query( $query );

     $query2 = array(
         'post_type'      => 'attachment',
         'cat' => 4,
         's' => $data['slug'],
         'post_status'    => 'inherit',
         'posts_per_page' => 8,
     );
     $videos = new WP_Query( $query2 );

     $query3 = array(
         'post_type'      => 'attachment',
         'cat' => 3,
         's' => $data['slug'],
         'post_status'    => 'inherit',
         'posts_per_page' => 4,
     );
     $files = new WP_Query( $query3 );

    $content['images'] = $images;
    $content['videos'] = $videos;
    $content['files'] = $files;
//    return $content;
    return render('parts/search_content.php' ,$content);
}

function paginateImages( $data ){
    $query = array(
        'post_type'      => 'attachment',
        'cat' => 2,
        'paged' => $data['slug'],
        'post_status'    => 'inherit',
        'posts_per_page' => 8,
    );
    $images = new WP_Query( $query );
    $content['images'] = $images;

    return render('parts/paginate/images.php' ,$content);
}

function paginateVideos( $data ){
    $query = array(
        'post_type'      => 'attachment',
        'cat' => 4,
        'paged' => $data['slug'],
        'post_status'    => 'inherit',
        'posts_per_page' => 8,
    );
    $videos = new WP_Query( $query );
    $content['videos'] = $videos;

    return render('parts/paginate/videos.php' ,$content);
}

function paginateFiles( $data ){
    $query = array(
        'post_type'      => 'attachment',
        'cat' => 3,
        'paged' => $data['slug'],
        'post_status'    => 'inherit',
        'posts_per_page' => 4,
    );
    $files = new WP_Query( $query );
    $content['files'] = $files;

    return render('parts/paginate/files.php' ,$content);
}