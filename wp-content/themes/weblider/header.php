<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title>
    Grafotronic - Digital Finishing Machines
  </title>
  <meta name="description" content="Booking Panel Grafotronic" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon.ico" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/swiper.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/lightcase.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/plugin.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/main.css">
  <?php wp_head(); ?>
</head>

<body>
  <?php global $current_user;
  wp_get_current_user(); ?>

  <div class="hamburger js-hamburger">
    <div class="hamburger__bar"></div>
  </div>
  <div class="mobile__menu">

    <nav class="mobile__nav">
      <ul class="nav-menu">
        <?php wp_nav_menu(array('theme_location' => 'main-menu')); ?>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="/">Book demo</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a href="/calendar">Calendar</a></li>
        <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads">Downloads</a></li> -->
        <?php

        if (get_userdata(get_current_user_id())->roles) {
          $user_role = get_userdata(get_current_user_id())->roles[0];
        } else {
          $user_role = null;
        }

        if ($user_role) {
          switch ($user_role) {
            case 'administrator':
            case 'um_manager':
        ?>
              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads-manager">Downloads</a></li>
            <?php
              break;
            case 'um_sales':
            ?>
              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads-sales">Downloads</a></li>
            <?php
              break;
            case 'um_service':
            ?>
              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads-service">Downloads</a></li>
        <?php
              break;
          }
        }
        ?>

      </ul>
    </nav>
  </div>
  <header style="background-color: black">
    <div class="container-fluid">
      <div class="header-container" style="position: relative;">
        <div class="header__logo">
          <a href="/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo@2.png" />
          </a>
        </div>
        <div class="desktop-wrapper">
          <div class="header__navigation">
            <nav class="navigation">
              <ul class="nav-menu">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="/">Book demo</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a href="/calendar">Calendar</a></li>
                <!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads">Downloads</a></li> -->
                <?php
                if ($user_role) {
                  switch ($user_role) {
                    case 'administrator':
                    case 'um_manager':
                ?>
                      <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads-manager">Downloads</a></li>
                    <?php
                      break;
                    case 'um_sales':
                    ?>
                      <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads-sales">Downloads</a></li>
                    <?php
                      break;
                    case 'um_service':
                    ?>
                      <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="/downloads-service">Downloads</a></li>
                <?php
                      break;
                  }
                }
                ?>
              </ul>
            </nav>
          </div>
          <?php if (is_user_logged_in()) { ?>
            <div class="user-info">
              <a href="/account">
                <p class="user-name"><?php echo $current_user->display_name . "\n"; ?></p>
              </a>
              <a class="user-link" href="<?php echo wp_logout_url(); ?>">
                <button class="btn user-btn">Logout</button>
              </a>
            </div>
          <?php } ?>

        </div>

      </div>

    </div>
    <script>
      const hamburger = document.querySelector('.js-hamburger');
      const body = document.querySelector('body');

      if (hamburger) {
        hamburger.addEventListener('click', () => {
          \
          console.log('działaj');
          hamburger.classList.toggle('is-active');
          body.classList.toggle('mobile__menu--open');
        })
      }
    </script>
  </header>