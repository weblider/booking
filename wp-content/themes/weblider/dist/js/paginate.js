//sensowny czas między klikami
var delayValue = 400;

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function submitData(method, searchValue, url, fill) {
    request = $.ajax({
        //uderzam do customowego routingu
        url: url + "/" + searchValue,
        type: method,
        fail: function () {},
        success: function (data) {
            //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
            console.log(data);
            $(fill).hide().fadeOut(250).html(data).fadeIn(350);
            //Więc wrzucam 'data' do tej klasy
        },
    });
}


$("#search_input").keyup(
    delay(function (event) {
        console.log($("#search_input").val());
        let searchValue = $("#search_input").val();
        let method = "POST";
        let fill = ".main-wrapper"
        let url = "/wp-json/wp/v1/search"
        submitData(method, searchValue, url, fill);
    }, delayValue)
);


$(".pagi-files").find(".pagi").on('click', function(event){
    event.preventDefault();
    $(".pagi-files").find(".pagi").removeClass('active-pagi');
    $(this).addClass('active-pagi');
    let searchValue = $(this).text();
    let method = $(".pagi-files").data('method');
    let fill = $(".pagi-files").data('fill');
    let url = $(".pagi-files").data('url');
    submitData(method, searchValue, url, fill);
})

$(".pagi-images").find(".pagi").on('click', function(event){
    event.preventDefault();
    $(".pagi-images").find(".pagi").removeClass('active-pagi');
    $(this).addClass('active-pagi');
    let searchValue = $(this).text();
    let method = $(".pagi-images").data('method');
    let fill = $(".pagi-images").data('fill');
    let url = $(".pagi-images").data('url');
    submitData(method, searchValue, url, fill);
})

$(".pagi-videos").find(".pagi").on('click', function(event){
    event.preventDefault();
    $(".pagi-videos").find(".pagi").removeClass('active-pagi');
    $(this).addClass('active-pagi')
    let searchValue = $(this).text();
    let method = $(".pagi-videos").data('method');
    let fill = $(".pagi-videos").data('fill');
    let url = $(".pagi-videos").data('url');
    submitData(method, searchValue, url, fill);
})

