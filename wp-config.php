<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// This enables debugging.
define('WP_DEBUG', true);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'admin_booking');

/** MySQL database username */
define('DB_USER', 'admin_booking');

/** MySQL database password */
define('DB_PASSWORD', 'NoweHaslo123@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k87I0q2Kfkxfcw4X0u7HM61m/kwaWFhn0ydxyvnsUDVsUvWubwRKmI9PBGMcF0Oel2nwGGfAvNEq9alQ1bGQLw==');
define('SECURE_AUTH_KEY',  '0eu/tav20df1QFAcHv1OSsdHAVzHTYglQu4ur+3Mk3r/5fj9Oq5DHU11bVfZefBJnPPuJdb8b3dlVlpx6+QNTw==');
define('LOGGED_IN_KEY',    'ALPsmWpa//CVPTIENDS3c+Qfmadvrwa5nheXW0qjXVcOuxoDDm04j3PuHieK2GdrxHyWRhl01aNCp5F2n/DoeA==');
define('NONCE_KEY',        'L06rArPMMWmEzxXxrMGVCVS91hsDb7AQDnO8iT5ncOg+WUQqzcaHL1F1kWh8wJp15mWquMGsA3qNRtiR5bUTtQ==');
define('AUTH_SALT',        '7jR+MuaMUGD1G0l3ZvWSQK7/xT4dw1oNV53tCo6iUhYp2pzBSk+NTz96/NGXPp2zXnor7kW9M7Wb2OLrk5xlAg==');
define('SECURE_AUTH_SALT', 'vpxUIqB4F9FUL48uoSUIr29VUV4dbhWAlbJUVXQF5wniDdjWf32vRG9Dm86FFqGw33IW84T9nY6E6yqdeu8wOQ==');
define('LOGGED_IN_SALT',   'g0O3RyAStpfWHIXUcUzJjrLubLFKqXdldnS88NXVwjPWzdxAyCxvzUq4QzGu/n1qJMiCR3XhxpQIEC8iyv1/ng==');
define('NONCE_SALT',       '9zEc7o3CvsGhVOKgFat+H+xe2Mm9pONAawJkLuNLbY0SLx+mPFCgVAqlGImoPDaEwvRXx+soGcnSopooariHMg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
